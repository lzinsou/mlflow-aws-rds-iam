"""Tracking and Model Registry plugin tests."""
import os
from unittest.mock import patch

import pytest

from mlflow_aws_rds_iam import model_registry, tracking

TRUEY = ("true", "1")

HOST = "localhost"
USERNAME = "user"

TOKEN = "t0k3n"
PASSWORD = "passw0rd"

ARTIFACT_KW = "default_artifact_root"
ARTIFACT_URI = "dummy://bucket"


@pytest.mark.parametrize(
    "uri,driver,host,port,username",
    [
        (
            f"postgresql://{USERNAME}@{HOST}:5432/mlflow",
            "postgresql",
            HOST,
            5432,
            USERNAME,
        ),
        (f"postgresql://{USERNAME}@{HOST}/mlflow", "postgresql", HOST, 5432, USERNAME),
        (f"mysql://{USERNAME}@{HOST}:3306/mlflow", "mysql", HOST, 3306, USERNAME),
        (f"mysql://{USERNAME}@{HOST}/mlflow", "mysql", HOST, 3306, USERNAME),
    ],
)
@patch("mlflow_aws_rds_iam.model_registry.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.model_registry.event")
@patch("mlflow_aws_rds_iam.model_registry.boto3.client")
def test_model_registry_rds_iam_auth(
    mocked_rds_client,
    mocked_events,
    mocked_super_store_init,
    uri,
    driver,
    host,
    port,
    username,
):
    """When no password is provided in the model registry URI, RDS IAM auth is used."""
    mocked_rds_client.return_value.generate_db_auth_token.return_value = TOKEN

    model_registry.RDSIAMStore(uri)

    # AWS RDS client is used to inject the token as password
    mocked_rds_client.return_value.generate_db_auth_token.assert_called_once_with(
        DBHostname=host,
        Port=port,
        DBUsername=username,
    )
    mocked_super_store_init.assert_called_once()
    assert mocked_super_store_init.call_args.args[0].startswith(
        f"{driver}://{username}:{TOKEN}@{host}"
    )

    # SSL is enabled
    assert "ssl" in mocked_super_store_init.call_args.args[0]

    # Token rotation listener is set up
    mocked_events.listen.assert_called_once()


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{USERNAME}@{HOST}:5432/mlflow",
        f"mysql://{USERNAME}@{HOST}:3306/mlflow",
    ],
)
@pytest.mark.parametrize("disable_db_ssl", TRUEY)
@patch("mlflow_aws_rds_iam.model_registry.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.model_registry.event")
@patch("mlflow_aws_rds_iam.model_registry.boto3.client")
def test_model_registry_rds_iam_auth_no_ssl(
    mocked_rds_client, mocked_events, mocked_super_store_init, uri, disable_db_ssl
):
    """With RDS IAM auth, a dedicated environment variable flag disables SSL."""
    mocked_rds_client.return_value.generate_db_auth_token.return_value = TOKEN

    with patch.dict(os.environ, {"MLFLOW_DISABLE_DB_SSL": disable_db_ssl}):
        model_registry.RDSIAMStore(uri)

    # SSL is disabled
    assert "ssl" not in mocked_super_store_init.call_args.args[0]


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{USERNAME}:{PASSWORD}@{HOST}:5432/mlflow",
        f"mysql://{USERNAME}:{PASSWORD}@{HOST}:3306/mlflow",
    ],
)
@patch("mlflow_aws_rds_iam.model_registry.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.model_registry.event")
@patch("mlflow_aws_rds_iam.model_registry.boto3.client")
def test_model_registry_password_auth(
    mocked_rds_client, mocked_events, mocked_super_store_init, uri
):
    """When a password is provided in the model registry URI, password auth is used."""
    model_registry.RDSIAMStore(uri)

    # SQLAlchemy store superclass is initialized with the same arguments as RDSIAMStore
    mocked_super_store_init.assert_called_once()
    assert mocked_super_store_init.call_args.args[0].startswith(uri)

    # SSL is enabled
    assert "ssl" in mocked_super_store_init.call_args.args[0]

    # AWS RDS client and SQLAlchemy events are unused
    mocked_rds_client.assert_not_called()
    mocked_events.listen.assert_not_called()


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{USERNAME}:{PASSWORD}@{HOST}:5432/mlflow",
        f"mysql://{USERNAME}:{PASSWORD}@{HOST}:3306/mlflow",
    ],
)
@pytest.mark.parametrize("disable_db_ssl", TRUEY)
@patch("mlflow_aws_rds_iam.model_registry.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.model_registry.event")
@patch("mlflow_aws_rds_iam.model_registry.boto3.client")
def test_model_registry_password_auth_no_ssl(
    mocked_rds_client, mocked_events, mocked_super_store_init, uri, disable_db_ssl
):
    """With password auth, a dedicated environment variable flag disables SSL."""
    with patch.dict(os.environ, {"MLFLOW_DISABLE_DB_SSL": disable_db_ssl}):
        model_registry.RDSIAMStore(uri)

    # SSL is disabled
    assert "ssl" not in mocked_super_store_init.call_args.args[0]


def test_model_registry_bad_db_type():
    """Ensure store initialization fails when a bad database type is provided."""
    with pytest.raises(ValueError) as excinfo:
        model_registry.RDSIAMStore("sqlite://")

    assert "Database driver is 'sqlite'" in str(excinfo.value)
    assert "only support drivers: postgresql, mysql" in str(excinfo.value)


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{HOST}:5432/mlflow",
        f"mysql://{HOST}:3306/mlflow",
    ],
)
def test_model_registry_no_user(uri):
    """Ensure store initialization fails when no user database is provided."""
    with pytest.raises(ValueError) as excinfo:
        model_registry.RDSIAMStore(uri)

    assert "requires a URI with a host and user" in str(excinfo.value)


@pytest.mark.parametrize(
    "uri,driver,host,port,username",
    [
        (
            f"postgresql://{USERNAME}@{HOST}:5432/mlflow",
            "postgresql",
            HOST,
            5432,
            USERNAME,
        ),
        (f"postgresql://{USERNAME}@{HOST}/mlflow", "postgresql", HOST, 5432, USERNAME),
        (f"mysql://{USERNAME}@{HOST}:3306/mlflow", "mysql", HOST, 3306, USERNAME),
        (f"mysql://{USERNAME}@{HOST}/mlflow", "mysql", HOST, 3306, USERNAME),
    ],
)
@patch("mlflow_aws_rds_iam.tracking.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.tracking.event")
@patch("mlflow_aws_rds_iam.tracking.boto3.client")
def test_tracking_rds_iam_auth(
    mocked_rds_client,
    mocked_events,
    mocked_super_store_init,
    uri,
    driver,
    host,
    port,
    username,
):
    """When no password is provided in the tracking URI, RDS IAM auth is used."""
    mocked_rds_client.return_value.generate_db_auth_token.return_value = TOKEN

    tracking.RDSIAMStore(uri, ARTIFACT_URI)

    # AWS RDS client is used to inject the token as password
    mocked_rds_client.return_value.generate_db_auth_token.assert_called_once_with(
        DBHostname=host,
        Port=port,
        DBUsername=username,
    )
    mocked_super_store_init.assert_called_once()
    assert mocked_super_store_init.call_args.args[0].startswith(
        f"{driver}://{username}:{TOKEN}@{host}"
    )
    assert mocked_super_store_init.call_args.kwargs[ARTIFACT_KW] == ARTIFACT_URI

    # SSL is enabled
    assert "ssl" in mocked_super_store_init.call_args.args[0]

    # Token rotation listener is set up
    mocked_events.listen.assert_called_once()


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{USERNAME}@{HOST}:5432/mlflow",
        f"mysql://{USERNAME}@{HOST}:3306/mlflow",
    ],
)
@pytest.mark.parametrize("disable_db_ssl", TRUEY)
@patch("mlflow_aws_rds_iam.tracking.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.tracking.event")
@patch("mlflow_aws_rds_iam.tracking.boto3.client")
def test_tracking_rds_iam_auth_no_ssl(
    mocked_rds_client, mocked_events, mocked_super_store_init, uri, disable_db_ssl
):
    """With RDS IAM auth, a dedicated environment variable flag disables SSL."""
    mocked_rds_client.return_value.generate_db_auth_token.return_value = TOKEN

    with patch.dict(os.environ, {"MLFLOW_DISABLE_DB_SSL": disable_db_ssl}):
        tracking.RDSIAMStore(uri, ARTIFACT_URI)

    # SSL is disabled
    assert "ssl" not in mocked_super_store_init.call_args.args[0]


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{USERNAME}:{PASSWORD}@{HOST}:5432/mlflow",
        f"mysql://{USERNAME}:{PASSWORD}@{HOST}:3306/mlflow",
    ],
)
@patch("mlflow_aws_rds_iam.tracking.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.tracking.event")
@patch("mlflow_aws_rds_iam.tracking.boto3.client")
def test_tracking_password_auth(
    mocked_rds_client, mocked_events, mocked_super_store_init, uri
):
    """With password auth, a dedicated environment variable flag disables SSL."""
    tracking.RDSIAMStore(uri, ARTIFACT_URI)

    # SQLAlchemy store superclass is initialized with the same arguments as RDSIAMStore
    mocked_super_store_init.assert_called_once()
    assert mocked_super_store_init.call_args.args[0].startswith(uri)
    assert mocked_super_store_init.call_args.kwargs[ARTIFACT_KW] == ARTIFACT_URI

    # SSL is enabled
    assert "ssl" in mocked_super_store_init.call_args.args[0]

    # AWS RDS client and SQLAlchemy events are unused
    mocked_rds_client.assert_not_called()
    mocked_events.listen.assert_not_called()


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{USERNAME}:{PASSWORD}@{HOST}:5432/mlflow",
        f"mysql://{USERNAME}:{PASSWORD}@{HOST}:3306/mlflow",
    ],
)
@pytest.mark.parametrize("disable_db_ssl", TRUEY)
@patch("mlflow_aws_rds_iam.tracking.SqlAlchemyStore.__init__")
@patch("mlflow_aws_rds_iam.tracking.event")
@patch("mlflow_aws_rds_iam.tracking.boto3.client")
def test_tracking_password_auth_no_ssl(
    mocked_rds_client, mocked_events, mocked_super_store_init, uri, disable_db_ssl
):
    """When a password is provided in the tracking URI, password auth is used."""
    with patch.dict(os.environ, {"MLFLOW_DISABLE_DB_SSL": disable_db_ssl}):
        tracking.RDSIAMStore(uri, ARTIFACT_URI)

    # SSL is disabled
    assert "ssl" not in mocked_super_store_init.call_args.args[0]


def test_tracking_bad_db_type():
    """Ensure store initialization fails when a bad database type is provided."""
    with pytest.raises(ValueError) as excinfo:
        tracking.RDSIAMStore("sqlite://", ARTIFACT_URI)

    assert "Database driver is 'sqlite'" in str(excinfo.value)
    assert "only support drivers: postgresql, mysql" in str(excinfo.value)


@pytest.mark.parametrize(
    "uri",
    [
        f"postgresql://{HOST}:5432/mlflow",
        f"mysql://{HOST}:3306/mlflow",
    ],
)
def test_tracking_no_user(uri):
    """Ensure store initialization fails when no user database is provided."""
    with pytest.raises(ValueError) as excinfo:
        tracking.RDSIAMStore(uri, ARTIFACT_URI)

    assert "requires a URI with a host and user" in str(excinfo.value)
