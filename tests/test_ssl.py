"""Test SSL utilities."""
from importlib.resources import as_file, files
from urllib.parse import urlencode

from sqlalchemy import make_url

from mlflow_aws_rds_iam.ssl import CERT_PATH, set_ssl_params

PACKAGE = "mlflow_aws_rds_iam"


def test_set_ssl_params_postgres():
    """SSL parameters are added to a Postgres URL."""
    uri = "postgresql://test"
    assert "sslmode=verify-ca" in str(set_ssl_params(make_url(uri)))
    with as_file(files(PACKAGE).joinpath(CERT_PATH)) as certfile:
        assert urlencode({"sslrootcert": str(certfile)}) in str(
            set_ssl_params(make_url(uri))
        )


def test_set_ssl_params_mysql():
    """SSL parameters are added to a MySQL URL."""
    uri = "mysql://test"
    assert "ssl-mode=VERIFY_CA" in str(set_ssl_params(make_url(uri)))
    with as_file(files(PACKAGE).joinpath(CERT_PATH)) as certfile:
        assert urlencode({"ssl-ca": str(certfile)}) in str(
            set_ssl_params(make_url(uri))
        )
