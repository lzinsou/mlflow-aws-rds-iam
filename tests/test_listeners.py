"""Test SQLAlchemy listeners."""
from unittest.mock import patch

from mlflow_aws_rds_iam.listeners import make_token_provider

TOKEN = "t0k3n"


class PostgresqlTestDialect:
    name = "postgresql"


@patch("mlflow_aws_rds_iam.model_registry.boto3.client")
def test_token_provider(mocked_rds_client):
    """Ensure the listener sets a token as the password in connection parameters."""
    mocked_rds_client.return_value.generate_db_auth_token.return_value = TOKEN

    token_provider = make_token_provider(mocked_rds_client.return_value)
    cparams = {"host": "test", "port": 5432, "user": "test_user"}
    token_provider(PostgresqlTestDialect, None, tuple(), cparams)

    assert ("password", TOKEN) in cparams.items()
